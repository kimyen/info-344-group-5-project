<?php
	session_start();
	// Connect to database 
    $dataBase = new PDO('mysql:host=vergil.u.washington.edu;port=10216;dbname=info344_project', 'Squirrel', 'pasta10216');
	
	$authorID = $_SESSION["id"];
    $quote = $_POST["quote"];
    
    $query = "INSERT INTO quotes (authorID, quote, date) VALUES (:authorID, :quote, NOW());";
    $statement = $dataBase->prepare($query); // Prepare the query
    $statement->execute(array(':authorID'=>$authorID, ':quote'=>$quote));
    $data = $statement->fetchAll(PDO::FETCH_ASSOC);
	
	# redirect to login.html 
	header("location:user.php");
	exit;
?>