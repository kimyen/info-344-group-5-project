var boo = new Boolean();

$(document).ready(function() {
	validate();
});

// Validates the form
function validate() {
	$("form").validate({
		rules: {
			user: {required: true},
			password: {required: true, minlength: 5}, 
			c_password: {required: true, equalTo: "#password", minlength: 5} 
		}
	});
	$('#user').blur(function() {
		if ($('#user').val() != ''){
			$.ajax('signup.php', {
				data: {username: $('#user').val()},
				type: 'get',
				dataType: 'json',
				success: check,
				error: function(request, type, errorThrown){
					alert('fail');
				}
			});
		}
	});
	$('#user').focus(function() {
		$('#error').empty();
	});
	$("form").submit(function() {
		return boo;
	});
}

// Checks if the username is unique
function check(data) {
	if (data.length == 0 || data == null){
		$('#error').empty();
		boo = true;
	} else {
		$('#error').text('Username is already taken!');
		boo = false;
	}
}
