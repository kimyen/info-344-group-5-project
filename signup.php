<?php
	session_start();
	if (!isset($_POST['user']) && !isset($_POST['password']) && !isset($_GET['username'])){ 
		// redirects to signup.html
		header("location:signup.html");
		exit;
	}
	$db = new PDO('mysql:host=vergil.u.washington.edu;port=10216;dbname=info344_project', 'Squirrel', 'pasta10216');

	switch ($_SERVER['REQUEST_METHOD']) {
		case 'GET':
			// Gets userID thats matches the given username
			if (isset($_GET['username'])){
				$user = $_GET['username'];
				$query = "SELECT u.userID FROM users u WHERE u.name=:name";
				$statement = $db->prepare($query); 
				$statement->execute(array(':name'=>$user));
				$data = $statement->fetchAll(PDO::FETCH_ASSOC);
				print json_encode($data);
			} 
			break;
	
		case 'POST':
			// Puts the username and password in the database
			if (isset($_POST['user']) && !empty($_POST['user']) && isset($_POST['password']) && !empty($_POST['password']) && isset($_POST['c_password']) && !empty($_POST['c_password'])){
				if ($_POST['password'] != $_POST['c_password']){
					print '400 Bad Request Password do not match';
					break;
				}
				if (strlen($_POST['password']) < 5){
					print '400 Bad Request Password invalid length';
					break;
				}
				$username = $_POST["user"];
				$password = $_POST["password"];
				$check = $db->prepare("SELECT u.userID FROM users u WHERE u.name=:name"); 
				$check->execute(array(':name'=>$username));
				$result = $check->fetchAll(PDO::FETCH_ASSOC);
			
				if (count($result) != 0){
					print '400 Bad Request Username not unique';
					break;
				}

				$sql = "INSERT INTO users (name, password) VALUES (:name, :password)";
				$query = $db->prepare($sql); 
				$query->execute(array(':name'=>$username, ':password'=>$password));
				
				// Check to see if the username and password match and return userid
				$query2 = "SELECT u.userID FROM users u WHERE u.name=:name AND u.password=:password;";
				$statement = $db->prepare($query2); // Prepare the query
				$statement->execute(array(':name'=>$username, ':password'=>$password));
				$data = $statement->fetchAll(PDO::FETCH_ASSOC);
			
				$id = $data[0][userID];
				
				$_SESSION["username"] = $username;
				$_SESSION["id"] = $id;
				
				# set time
				setcookie("login_time",time());

				# redirect to index.php
				header("location:index.php");
				exit;
			} else {
				print '400 Bad Request';
			}
			break;
	}
?>
