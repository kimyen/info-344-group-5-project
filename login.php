<?php
	session_start();
	
	$username=$_POST["username"];
	$password=$_POST["password"];
	
	if (!isset($username) or !isset($password) or (strlen($username)==0) or (strlen($password)==0) ) {
		
		# redirect to login.html 
		header("location:login.html");
		exit;
	}
	
	// Connect to database 
    $dataBase = new PDO('mysql:host=vergil.u.washington.edu;port=10216;dbname=info344_project', 'Squirrel', 'pasta10216');
	
	# check to see if the username and password match and return userid
    $query = "SELECT u.userID FROM users u WHERE u.name=:name AND u.password=:password;";
    $statement = $dataBase->prepare($query); // Prepare the query
    $statement->execute(array(':name'=>$username, ':password'=>$password));
    $data = $statement->fetchAll(PDO::FETCH_ASSOC);
    
	$id = $data[0][userID];
	
	if (!$id) {
		# redirect to login.html 
		header("location:login.html");
		exit;
		
	} else {
		# start a new session
	
		session_destroy();
		session_regenerate_id(TRUE);   # flushes out session ID number
		session_start();
		
		# set session variables
		$_SESSION["username"] = $username;
		$_SESSION["id"] = $id;
		
		# set time
		setcookie("login_time",time());
	
		# redirect to index.php
		header("location:index.php");
		exit;
	}

?>