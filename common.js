// This variable keeps track of the page
var current = 0;

$(document).ready(function(){
	$("#user").mouseover(function(){
		$("#userinfo").removeClass("hidden");
	});
	$("#user").mouseout(function(){
		$("#userinfo").addClass("hidden");
	});
	var button = $('#buttons').children('div .button');
	var group = $('#content').children('div .group');
	setup(group, button);
	$("#prev").click(function (){
		current--;
		change(group, button);
	});
	$("#next").click(function (){
		current++;
		change(group, button);
	});
});

// This function changes the page of tweets
function change(group, button){
	if (current > group.length - 1){
		current = 0;
	} else if (current < 0){
		current = group.length - 1;
	}
	for(var i = group.length - 1; i >= 0; i--) {
		if (current == i && $(group[i]).hasClass('hidden')) {
			$(group[i]).removeClass('hidden').addClass('unhidden');
			$(button[i]).addClass('active');
		} else if ($(group[i]).hasClass('unhidden') && current != i) {
			$(group[i]).removeClass('unhidden').addClass('hidden');
			$(button[i]).removeClass('active');
		}
	}
}

// This function sets up the pages of tweets
function setup (group, button){
	for (var i = 0; i < group.length; i++){
		if(i == 0){
			$(group[i]).addClass('unhidden');
		} else {
			$(group[i]).addClass('hidden');
		}
	}
	$(button[0]).addClass('active');
    $(button).on("click", function() {
		current = $(this).attr('num');
		change(group, button);
    });
}