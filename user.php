<?php 
	session_start(); 
	
	# check if the user enter the page illegally
	if (!isset($_SESSION["username"])) {
		
		# redirect to login.html 
		header("location:login.html");
		exit;
		
	} 
	
	# get user info from session
	$username = $_SESSION["username"];
	
	# create a link for the user page
	$userpage = "user.php?username=" . $username;
	
	# get the user who is looked for
	$user = $_GET["username"];
	
	if (!isset($user)){
		$user = $username;
	}
	
	// Connect to database 
    $dataBase = new PDO('mysql:host=vergil.u.washington.edu;port=10216;dbname=info344_project', 'Squirrel', 'pasta10216');

    $query = "SELECT u.name, q.quote, q.date FROM quotes q JOIN users u ON u.userID = q.authorID WHERE u.name = :name ORDER BY date DESC;";
    $statement = $dataBase->prepare($query); // Prepare the query
    $statement->execute(array(':name'=>$user));
    $data = $statement->fetchAll(PDO::FETCH_ASSOC);
	
	if (!$data) {
		# redirect to finduser.html
		header("location:finduser.html");
		exit;	
	}
	
	# get log-in time 
	$time_format="%A %B %e, %Y %I:%M%p";
	$login_time=$_COOKIE["login_time"];
	$login_time=strftime($time_format,$login_time);
		
?>
	<!DOCTYPE html>
		<!-- user page -->
		<head>
		    <title><?= $username ?></title>
		    <link href='style.css' rel='stylesheet' type='text/css'>
			<link rel="icon" href="favicon.ico" type="image/x-icon">
		    <script type='text/javascript' src='http://code.jquery.com/jquery-latest.min.js'></script>
		    <script type='text/javascript' src='common.js'></script>
		</head>
		
		<body>
			<a href="index.php" ><img id="logo" src="logo.png" alt="logo" ></a>
			<header>
				<!-- name of the site, user's options  -->
				
				<div id = "user">
					<a href="user.php"><span><?= $username ?></span></a>
					
					<!-- will be shown when hover -->
					<ul class = "hidden" id = "userinfo">
						<li><a href=<?= '"' . $userpage . '"'?> >My quotes</a></li>
						<li><a href="finduser.html">Find user</a></li>
						<li><a href="addquotes.html">Add quotes</a></li>
						<li><a href="logout.php">Log out</a></li>	
					</ul>	
					
				</div>	
				
				<a href="index.php" ><h1>Quotes</h1></a>		
			</header>
			
			<div id="logininfo">
				<p>Log in since <?=$login_time?></p>
			</div>
			<h1>Quotes posted by <?= $user ?></h1>
			<div id="content">
				<?php
					foreach ($data as $row) {
        		?>
        		
				<div class = "post">
					
					<div class = "date">
						<!-- date posted -->
						<p><?=$row["date"]?></p>
					</div>
					
					<div class = "author">
						<!-- name/ nick name -->
						<p><?=$row["name"]?> said, </p>
					</div>
					
					<div class = "quote">
						<p>&quot;<?=$row["quote"]?>&quot;</p>
					</div>
					
				</div> <!-- end post -->
				
				<?php
   					 }
   				?>
			</div>
			
			<footer>
				<!-- copyright - group 5 -->
				<p>Made by <strong>Jeff Giorgi</strong>, <strong>Mackenzie Yu</strong>, and <strong>KimYen Truong</strong> </p>
			</footer>
		</body>
	</html>