<?php
	# if there is a session, log-in succeed 
	
	session_start(); 
	# check if the user enter the page illegally
	if (!isset($_SESSION["username"])) {
		
		# redirect to login.html 
		header("location:login.html");
		exit;
		
	} 
	# get user info from session
	$username=$_SESSION["username"];
	
	# get log-in time 
	$time_format="%A %B %e, %Y %I:%M%p";
	$login_time=$_COOKIE["login_time"];
	$login_time=strftime($time_format,$login_time);
	
	# create a link for the user page
	$userpage = "user.php?username=" . $username;
		
?>

	<!DOCTYPE html>
		<!-- public page -->
		<head>
		    <title>Group 5</title>
		    <link href='style.css' rel='stylesheet' type='text/css'>
		    <script type='text/javascript' src='http://code.jquery.com/jquery-latest.min.js'></script>
		    <script type='text/javascript' src='common.js'></script>
		</head>
		
		<body>
			<a href="index.php" ><img id="logo" src="logo.png" alt="logo" ></a>
			<header>
				<!-- name of the site, user's options  -->
				
				<div id = "user">
					<a href="user.php"><span><?= $username ?></span></a>
					
					<!-- will be shown when hover -->
					<ul class = "hidden" id = "userinfo">
						<li><a href=<?= '"' . $userpage . '"'?> >My quotes</a></li>
						<li><a href="finduser.html">Find user</a></li>
						<li><a href="addquotes.html">Add quotes</a></li>
						<li><a href="logout.php">Log out</a></li>	
					</ul>	
					
				</div>	
				
				<a href="index.php" ><h1>Quotes</h1></a>		
			</header>
			
			<div id="content">
				
				<div id="logininfo">
					<p>Log in since <?=$login_time?></p>
				</div>
				<h1>All quotes</h1>
				<?php
					// Connect to database 
    				$dataBase = new PDO('mysql:host=vergil.u.washington.edu;port=10216;dbname=info344_project', 'Squirrel', 'pasta10216');
					
					//load all quotes "SELECT u.name, q.quote FROM quotes q JOIN users u ON u.userID = q.authorID;"
				    $query = "SELECT u.name, q.quote, q.date FROM quotes q JOIN users u ON u.userID = q.authorID ORDER BY date DESC;";
				    $statement = $dataBase->prepare($query); // Prepare the query
				    $statement->execute(array());
				    $data = $statement->fetchAll(PDO::FETCH_ASSOC);
					
					// Number of post per page
					$NUM_POST = 5;
					
					// This puts the data on to the page 
					for ($i = 0; $i < ceil(count($data) / $NUM_POST); $i++){
				?>		
						<div class='group'>
						
				<?php
						for ($j = 0; $j < $NUM_POST; $j++){
							$row = $data[$j + ($i * $NUM_POST)];
							if ($row["date"] != NULL && $row["name"] != NULL && $row["quote"] != NULL){
					?>
								<div class = "post">
									
									<div class = "date">
										<!-- date posted -->
										<p><?=$row["date"]?></p>
									</div>
									
									<div class = "author">
										<!-- name/ nick name -->
										<p><a href='user.php?username=<?=$row["name"]?>'><?=$row["name"]?></a> said, </p>
									</div>
									
									<div class = "quote">
										<p>&quot;<?=$row["quote"]?>&quot;</p>
									</div>
									
								</div> <!-- end post -->
								
				<?php		} 
						}
					?>		
						</div>
				<?php
					}
				?>
				
			</div>
			<div id='buttons'>
				<div id="prev">
					<p><</p>
				</div>
				<?php
					// This puts the buttons on to the page
					for ($i = 1; $i <= ceil(count($data) / $NUM_POST); $i++){
				?>
						<div class='button' num='<?php print $i-1 ?>'>
							<span><?php print $i ?></span>
						</div>
				<?php
					}
				?>
				<div id="next">
					<p>></p>
				</div>
			</div>
			<footer>
				<!-- copyright - group 5 -->
				<p>Made by <strong>Jeff Giorgi</strong>, <strong>Mackenzie Yu</strong>, and <strong>KimYen Truong</strong> </p>
			</footer>
		</body>
	</html>